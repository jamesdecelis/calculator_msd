package mt.edu.mcast.calculator_msd;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView tv;
    int num1, num2;
    char operation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv = findViewById(R.id.txtvDisplay);
    }

    public void numberClicked(View v){

        Button btn = (Button)v;

        if(tv.getText().equals("0")){
            tv.setText(btn.getText().toString());
        }else {
            tv.setText(tv.getText() + btn.getText().toString());
        }

    }

    public void operationClicked(View v){

        num1 = Integer.parseInt(tv.getText().toString());

        Button btn = (Button)v;
        operation = btn.getText().charAt(0);

        tv.setText("0");

    }

    public void equalsClicked(View v){
        num2 = Integer.parseInt(tv.getText().toString());

        int ans = 0;
        switch(operation){
            case '+' :
                ans = num1 + num2;
                break;
            case '-' :
                ans = num1 - num2;
                break;
            case '*' :
                ans = num1 * num2;
                break;
            case '/' :
                ans = num1 / num2;
                break;
        }

        tv.setText(String.valueOf(ans));

    }
}
